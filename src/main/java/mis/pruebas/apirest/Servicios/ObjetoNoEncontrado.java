package mis.pruebas.apirest.Servicios;

public class ObjetoNoEncontrado extends RuntimeException {
    public ObjetoNoEncontrado(String message) {
        super(message);
    }
}
